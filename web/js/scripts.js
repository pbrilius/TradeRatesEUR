$(function(){
	$("#cdate").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange:'c:c+4',
		dateFormat: "yy-mm-dd"
	});
	$('#cForm').submit(function(e){
		e.preventDefault();
		e.stopPropagation();
		var res=parseFloat($('#cSum').val())/parseFloat($('#'+$('#cCurrencyF option:selected').val()).html());
		res=res*parseFloat($('#'+$('#cCurrencyT option:selected').val()).html());
		res=Math.round(res*100)/100;
		$('#DCTrade form.well .result').remove();
		$('#DCTrade form.well').append('<span class="result" style="font">Result: '+res+$('#cCurrencyT option:selected').html()+'</span>');
	})
});


