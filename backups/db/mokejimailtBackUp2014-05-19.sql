CREATE DATABASE  IF NOT EXISTS `mokejimailt` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mokejimailt`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: mokejimailt
-- ------------------------------------------------------
-- Server version	5.6.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `rate` float NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` VALUES (161,'USD',1.3696,'2014-05-16 15:00:00'),(162,'JPY',138.95,'2014-05-16 15:00:00'),(163,'BGN',1.9558,'2014-05-16 15:00:00'),(164,'CZK',27.443,'2014-05-16 15:00:00'),(165,'DKK',7.4646,'2014-05-16 15:00:00'),(166,'GBP',0.8148,'2014-05-16 15:00:00'),(167,'HUF',305.93,'2014-05-16 15:00:00'),(168,'LTL',3.4528,'2014-05-16 15:00:00'),(169,'PLN',4.1904,'2014-05-16 15:00:00'),(170,'RON',4.4357,'2014-05-16 15:00:00'),(171,'SEK',9.0122,'2014-05-16 15:00:00'),(172,'CHF',1.221,'2014-05-16 15:00:00'),(173,'NOK',8.1505,'2014-05-16 15:00:00'),(174,'HRK',7.5938,'2014-05-16 15:00:00'),(175,'RUB',47.692,'2014-05-16 15:00:00'),(176,'TRY',2.8755,'2014-05-16 15:00:00'),(177,'AUD',1.4633,'2014-05-16 15:00:00'),(178,'BRL',3.0353,'2014-05-16 15:00:00'),(179,'CAD',1.4897,'2014-05-16 15:00:00'),(180,'CNY',8.5379,'2014-05-16 15:00:00'),(181,'HKD',10.6169,'2014-05-16 15:00:00'),(182,'IDR',15600.1,'2014-05-16 15:00:00'),(183,'ILS',4.7398,'2014-05-16 15:00:00'),(184,'INR',80.6914,'2014-05-16 15:00:00'),(185,'KRW',1403.58,'2014-05-16 15:00:00'),(186,'MXN',17.7336,'2014-05-16 15:00:00'),(187,'MYR',4.4189,'2014-05-16 15:00:00'),(188,'NZD',1.5862,'2014-05-16 15:00:00'),(189,'PHP',60.057,'2014-05-16 15:00:00'),(190,'SGD',1.7136,'2014-05-16 15:00:00'),(191,'THB',44.512,'2014-05-16 15:00:00'),(192,'ZAR',14.2219,'2014-05-16 15:00:00');
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `session_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `modified` int(11) DEFAULT NULL,
  `data` text,
  `lifetime` int(11) DEFAULT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(250) NOT NULL,
  `lastName` varchar(350) NOT NULL,
  `email` varchar(120) NOT NULL,
  `created` datetime NOT NULL,
  `password` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Povilas','Garnys','pbrilius@groundin.com','2014-05-18 17:48:26','fc7dbf2b4827ed3f9a0b0ebc9fc79df47854e0d974d7421b68ff9214bf6f17370482766011863ef14dd471d9fe2adf3013c6a673211afe270b23215a8c7bc718'),(2,'Povilas','Garnys','pbrilius1@groundin.com','2014-05-18 17:49:48','fc7dbf2b4827ed3f9a0b0ebc9fc79df47854e0d974d7421b68ff9214bf6f17370482766011863ef14dd471d9fe2adf3013c6a673211afe270b23215a8c7bc718'),(3,'Povilas','Garnys','pbrilius2@groundin.com','2014-05-18 17:50:53','fc7dbf2b4827ed3f9a0b0ebc9fc79df47854e0d974d7421b68ff9214bf6f17370482766011863ef14dd471d9fe2adf3013c6a673211afe270b23215a8c7bc718'),(4,'Povilas','Garnys','pbrilius3@groundin.com','2014-05-18 17:53:24','fc7dbf2b4827ed3f9a0b0ebc9fc79df47854e0d974d7421b68ff9214bf6f17370482766011863ef14dd471d9fe2adf3013c6a673211afe270b23215a8c7bc718'),(5,'Povilas','Garnys','pbrilius4@groundin.com','2014-05-18 17:57:47','fc7dbf2b4827ed3f9a0b0ebc9fc79df47854e0d974d7421b68ff9214bf6f17370482766011863ef14dd471d9fe2adf3013c6a673211afe270b23215a8c7bc718'),(6,'Povilas','Garnys','pbrilius11@groundin.com','2014-05-18 18:31:03','ICTiscool45.'),(7,'Povilas','Garnys','pbrilius12@groundin.com','2014-05-18 18:33:52','g9WD90aTrcvOli+4OcN9d0tEpkmGtlPP+VU0wOwk9N5gwneRbqEAIfu8+andq1c/yOnF0LymEMGIBQH8J4toTA=='),(8,'Povilas','Garnys','pbrilius13@groundin.com','2014-05-18 18:53:10','/H2/K0gn7T+aCw68n8ed9HhU4Nl010IbaP+SFL9vFzcEgnZgEYY+8U3Ucdn+Kt8wE8amcyEa/icLIyFajHvHGA=='),(9,'Povilas','Garnys','pbrilius14@groundin.com','2014-05-19 00:55:38','/H2/K0gn7T+aCw68n8ed9HhU4Nl010IbaP+SFL9vFzcEgnZgEYY+8U3Ucdn+Kt8wE8amcyEa/icLIyFajHvHGA==');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-19  1:02:59
