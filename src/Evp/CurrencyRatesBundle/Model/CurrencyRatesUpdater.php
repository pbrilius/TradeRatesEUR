<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Evp\CurrencyRatesBundle\Model;

use Evp\UserBundle\Entity\Currency;

/**
 * Description of CurrencyRatesUpdater
 *
 * @author baronas
 */
class CurrencyRatesUpdater
{
    public function updateCurrencyRates($em, $xmlUrl)
    {
        $em = $this->getDoctrine()->getManager();
        $outdated = $em->getRepository('EvpUserBundle:Currency')
            ->getOutdated();
        
        if (empty($outdated)){
            $co=$em->getRepository('EvpUserBundle:Currency')->countEntries();
            if ((int)$co[0][1]!=0)
                return;
            else{
                $xml=simplexml_load_file($xmlUrl);
                $updated=$xml->Cube->Cube['time'].' 15:00:00';
                foreach($xml->Cube->Cube->Cube as $rate){
                    $c=new Currency();
                    $c->setCode($rate['currency'])
                        ->setRate($rate['rate'])
                        ->setUpdated($updated);
                    $em->persist($c);
                    $em->flush();
                }
                return;
            }
        }   
        $xml=simplexml_load_file($this->container->getParameter('ECB.XMLURL'));
        $updated=$xml->Cube->Cube['time'].' 15:00:00';
        foreach($xml->Cube->Cube->Cube as $rate){
            $arr[(string)$rate['currency']]=(float)$rate['rate'];
        }
        $codes=array_keys($arr);
        foreach ($outdated as $row){
            if (!in_array($row->getCode(), $codes))
                continue;
            $row->setRate($arr[$row->getCode()]);
            $row->setUpdated($updated);
        }
        $em->flush();
    }
}
