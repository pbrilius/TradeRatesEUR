<?php

namespace Evp\CurrencyRatesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('EvpCurrencyRatesBundle:Default:index.html.twig', array('name' => $name));
    }
}
