<?php

namespace Evp\UserBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use Evp\UserBundle\Entity\User;
/**
 * Description of Registration
 *
 * @author Baronas Ponas
 */
class Registration {
	/**
     * @Assert\Type(type="Evp\UserBundle\Entity\User")
     * @Assert\Valid()
     */
    protected $user;

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}
