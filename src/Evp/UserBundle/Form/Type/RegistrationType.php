<?php
namespace Evp\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType {
	
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('user', new UserType(), array('label'=>false));
        $builder->add('Register', 'submit', array('attr'=>array('class'=>'btn btn-lg btn-primary btn-block register')));
    }

    public function getName()
    {
        return 'registration';
    }
}
