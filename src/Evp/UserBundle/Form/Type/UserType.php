<?php

namespace Evp\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder->add('firstName', 'text', array(
			'attr'=>array('class'=>'form-control', 'placeholder'=>'First name'),
			'label' => false)
		);
		$builder->add('lastName', 'text', array(
			'attr'=>array('class'=>'form-control', 'placeholder'=>'Last name'),
			'label' => false)
		);
        $builder->add('email', 'email', array(
			'attr'=>array('class'=>'form-control', 'placeholder'=>'Email'),
			'label' => false));
		$builder->add('email', 'email', array(
			'attr'=>array('class'=>'form-control', 'placeholder'=>'Email'),
			'label' => false));
		
        $builder->add('password', 'repeated', array(
			'first_name'  => 'password',
			'second_name' => 'confirm',
			'type'        => 'password',
			'first_options'=>array(
				'attr'=>array('class'=>'form-control', 'placeholder'=>'Password'),
				'label' => false
			),
			'second_options'=>array(
				'attr'=>array('class'=>'form-control', 'placeholder'=>'Repeat password'),
				'label' => false
			),
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Evp\UserBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'user';
    }
}
