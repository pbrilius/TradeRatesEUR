<?php

namespace Evp\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Evp\UserBundle\Entity\Currency;
use Evp\UserBundle\Form\Type\RegistrationType;
use Evp\UserBundle\Form\Model\Registration;
use Evp\CurrencyRatesBundle\Model\CurrencyRatesUpdater;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('EvpUserBundle:Default:index.html.twig', array('name' => $name));
    }
	
	public function ctradeAction()
	{
		if (!$this->get('security.context')->isGranted('ROLE_USER'))
			throw new AccessDeniedException();
		
		$user = $this->getUser();
		$em = $this->getDoctrine()->getManager();
		$xmlUrl = $this->container->getParameter('ECB.XMLURL');
		$currencyUpdated = new CurrencyRatesUpdater();
		$currencyUpdated->updateCurrencyRates($em, $xmlUrl);
		$em = $this->getDoctrine()->getManager();
		$currencies = $em->getRepository('EvpUserBundle:Currency')->getCurrencies();
		return $this->render(
				'EvpUserBundle:User:ctrade.html.twig',
				array(
					'user' => $user,
					'currencies' => $currencies
				)
			);
		
	}
	
	public function loginAction(Request $request)
	{

		$session=$request->getSession();
		if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR))
			$error=$request->attributes->get(SecurityContextInterface::AUTHENTICATION_ERROR);
		else{
			$error=$request->attributes->get(SecurityContextInterface::AUTHENTICATION_ERROR);
			$session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
		}
		return $this->render(
				'EvpUserBundle:User:login.html.twig',
				array(
					'last_username' => $session->get(SecurityContextInterface::LAST_USERNAME),
					'error' => $error,
				)
		);
			
	}
	
	public function registerAction()
	{
		$registration=new Registration();
		$form=$this->createForm(new RegistrationType(), $registration, array(
			'action'=>$this->generateUrl('user_create')
		));
		return $this->render(
				'EvpUserBundle:User:register.html.twig',
				array('form'=>$form->createView())
		);
	}
	
	public function createAction(Request $request)
	{
		$em=$this->getDoctrine()->getManager();
		
		$form=$this->createForm(new RegistrationType(), new Registration());
		$form->handleRequest($request);
		if ($form->isValid()){
			$registration=$form->getData();
			$user=$registration->getUser();
			$factory = $this->get('security.encoder_factory');
			$encoder = $factory->getEncoder($user);
			$password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
			$user->setPassword($password);
			$user->setCreated(date('Y-m-d H:i:s'));
			$em->persist($user);
			$em->flush();
			
			return $this->redirect($this->generateUrl('evp_user_homepage'));
		}
		return $this->render(
				'EvpUserBundle:User:register.html.twig',
				array('form'=>$form->createView())
		);
	}	
}
